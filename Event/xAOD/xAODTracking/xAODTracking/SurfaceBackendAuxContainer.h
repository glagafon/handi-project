/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_SURFACEBACKENDAUXCONTAINER_H
#define XAODTRACKING_SURFACEBACKENDAUXCONTAINER_H

#include "xAODTracking/versions/SurfaceBackendAuxContainer_v1.h"

namespace xAOD {
  typedef SurfaceBackendAuxContainer_v1 SurfaceBackendAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::SurfaceBackendAuxContainer , 1251423340 , 1 )
#endif